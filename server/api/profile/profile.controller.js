var User = require("../../database").User;
var AuthProvider = require("../../database").AuthProvider;
var Profile = require("../../database").Profile;
var config = require('../../config')

/* FOR UPLOAD/DOWNLOAD OF PROFILE IMAGES */
/* 
var AWS = require('aws-sdk'); 
AWS.config.region = config.aws.region;
var s3Bucket =  new AWS.S3({
    params:  {
        Bucket: config.aws.bucket
    }
});
*/

exports.get = function (req, res) {
    Profile
        .findById(req.params.id)
        .then(function (profile) {

            if (!profile) {
                handler404(res);
            }

            res.json(profile);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.create = function (req, res) {
    Profile
        .create({
            url: req.body.url,
            userId: req.user.id
        })
        .then(function (profile) {
            res.json(profile);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(500)
                .json({error: true});
        });
};

exports.update = function (req, res) {
    Profile
        .findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function (profile) {
            profile
                .update(req.body)
                .then(function () {
                    res.json(profile)
                })
                .catch(function () {

                });
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};


exports.remove = function (req, res) {
    Profile
        .findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function (profile) {
            if (profile) {

            }
        })
        .catch(function () {

        });
};

/* FOR FUTURE PROFILE PHOTO */
/*
exports.showImage = function(req, res){
    console.log(req.params.url);
    console.log(config.aws.url);
    console.log(config.aws.bucket);
    var params = {Bucket: config.aws.bucket , Key: req.params.url};
    var stream = s3Bucket.getObject(params).createReadStream();
    stream.pipe(res);
}; */

function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}
