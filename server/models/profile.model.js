var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('profile', {
        userID: {
            type : Sequelize.STRING,
            allowNull: true
        },
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        score: Sequelize.INTEGER(5),
        userTitle: Sequelize.STRING,
        userBio: Sequelize.TEXT,
        // user_achievement: Sequelize.STRING,
        // user_badges: Sequelize.STRING,
    });
};