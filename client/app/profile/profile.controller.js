(function () {

    angular
        .module("upgrad8app")
        .controller("ProfileCtrl", ["UserAPI", "$scope", ProfileCtrl]);

    function ProfileCtrl(UserAPI, $http, $scope) {
        var vm = this;
        vm.sociallogins = [];
        UserAPI.getLocalProfile().then(function (result) {
            //console.log(result.data);
            vm.localProfile = result.data;
            
        });

        UserAPI.getAllSocialLoginsProfile().then(function (result) {
            vm.sociallogins = result.data;
        });
    }
    
    // function editProfile(UserAPI, $http, $scope){
    //     $scope.items = [{ dummyData: "bar" }, { dummyData: "bar" }, { dummyData: "baz" }];
    //     $scope.editEnabled = false;
    //     $scope.dummydata = "This is dummy data";
    //     $scope.editData = function (item) {
    //         item.editEnabled = true;
    //     };
    
    //     $scope.saveData = function (item) {
    //         item.editEnabled = false;
    //     };
    
    //     $scope.disableEdit = function () {
    //         $scope.editEnabled = false;
    //     };

    // }
})();

