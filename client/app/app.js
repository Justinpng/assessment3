(function () {
    angular
        .module("upgrad8app", [
            "ngFileUpload",
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "ngProgress",
            "ngMessages",
            "data-table"
        ]); 
})();