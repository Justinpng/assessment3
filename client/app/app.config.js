(function () {
    angular
        .module("upgrad8app")
        .config(upgrad8appConfig);
    upgrad8appConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function upgrad8appConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/users/login.html"
                    }
                },
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "content": {
                        templateUrl: "../app/users/register.html"
                    }
                },
                controller: 'RegisterCtrl',
                controllerAs: 'ctrl'
            })
            .state("ResetPassword", {
                url: "/ResetPassword",
                views: {
                    "content": {
                        templateUrl: "../app/users/reset-password.html"
                    }
                },
                controller: 'ResetPasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state("ChangeNewpassword", {
                url: "/changeNewpassword?token",
                views: {
                    "content": {
                        templateUrl: "../app/users/change-new-password.html"
                    }
                },
                controller: 'ChangeNewPasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state("MyAccount", {
                url: "/MyAccount",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'MyAccountCtrl',
                controllerAs: 'ctrl'
            })
            .state("ChangePassword", {
                url: "/ChangePassword",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/changePassword.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ChangePasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state('upgrad8', {
                url: '/upgrad8',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/upgrad8.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('aboutus', {
                url: '/aboutus',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/aboutus.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'AboutUsCtrl',
                controllerAs: 'ctrl'
            })
            .state('badges', {
                url: '/badges',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/badges.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'GuestListCtrl',
                controllerAs: 'ctrl'
            })
            .state('achievements', {
                url: '/achievements',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/achievements.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'SeatingsCtrl',
                controllerAs: 'ctrl'
            })
            .state('profile', {
                url: '/profile',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('back', {
                url: '/back',
                templateUrl: './app/users/login.html',
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('comments', {
                url: '/comments/:data',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/comment.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'CommentDetailCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/signIn");


    }
})();
